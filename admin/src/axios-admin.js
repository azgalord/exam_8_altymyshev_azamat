import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://burger-project-azamat60.firebaseio.com/contacts-page/'
});

export default instance;
