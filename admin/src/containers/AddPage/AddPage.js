import React, {Component} from 'react';
import Form from "../../components/Form/Form";
import {connect} from 'react-redux';

import {saveContact} from "../../store/actions/admin";

import './AddPage.css';

const INITIAL_INPUT_VALUES = {
  name: '',
  phone: '',
  email: '',
  photo: '',
};

class AddPage extends Component {
  state = {
    inputValues: {
      name: '',
      phone: '',
      email: '',
      photo: '',
    },
  };

  inputChangeHandler = event => {
    this.setState({inputValues: {
        ...this.state.inputValues,
        [event.target.name] : event.target.value,
    }});
  };

  saveContact = (event) => {
    event.preventDefault();
    this.props.saveContact(this.state.inputValues);
    this.props.history.replace('/');
  };

  goBack = (event) => {
    event.preventDefault();

    this.setState({inputValues: INITIAL_INPUT_VALUES});
    this.props.history.replace('/');
  };

  render() {
    return (
      <div className="AddPage">
        <Form
          formTitle="Add new contact"
          inputValues={this.state.inputValues}
          inputChange={(event) => this.inputChangeHandler(event)}
          photoReview={this.state.inputValues.photo}
          saveButton={event => this.saveContact(event)}
          backButton={(event) => this.goBack(event)}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  saveContact: (contact) => dispatch(saveContact(contact))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddPage);
