import React, {Component} from 'react';
import Form from "../../components/Form/Form";

import {connect} from 'react-redux';
import {fetchOneContact, sendChangedContact} from "../../store/actions/admin";

class EditPage extends Component {
  state = {
    inputValues: {
      name: '',
      phone: '',
      email: '',
      photo: '',
    },
  };

  componentDidMount() {
    this.props.fetchOneContact(this.props.match.params.id);
  }

  componentDidUpdate() {
    if (!this.state.inputValues.name.length) {
      this.setState({inputValues : this.props.contact});
    }
  }

  inputChangeHandler = event => {
    console.log(event.target.value);
    this.setState({inputValues: {
        ...this.state.inputValues,
        [event.target.name] : event.target.value,
      }});
  };

  goBack = (event) => {
    event.preventDefault();
    this.props.history.replace('/');
  };

  saveChangedContactFunc = (event) => {
    event.preventDefault();
    this.props.sendChangedContact(this.props.match.params.id, this.state.inputValues);
    this.props.history.replace('/');
  };

  render() {
    if (!this.props.contact) {
      return (<div className="Loading">Loading...</div>);
    }
    return (
      <div className="EditPage">
        <Form
          formTitle="Edit contact"
          inputValues={this.state.inputValues ? this.state.inputValues : ''}
          inputChange={event => this.inputChangeHandler(event)}
          photoReview={this.state.inputValues ? this.state.inputValues.photo : ''}
          saveButton={(event) => this.saveChangedContactFunc(event)}
          backButton={(event) => this.goBack(event)}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  contact: state.oneContact,
});

const mapDispatchToProps = dispatch => ({
  fetchOneContact: id => dispatch(fetchOneContact(id)),
  sendChangedContact: (id, data) => dispatch(sendChangedContact(id, data))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditPage);
