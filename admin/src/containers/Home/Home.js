import React, {Component} from 'react';

import {connect} from 'react-redux';
import {deleteContact, fetchContacts} from "../../store/actions/admin";
import HomeItem from "../../components/HomeItem/HomeItem";

import './Home.css';
import Modal from "../../components/UI/Modal/Modal";

class Home extends Component {
  state = {
    modalVisible: false,
    activeId: '',
    contact: null,
  };

  toggleModal = (id, contact) => {
    this.setState({
      contact: contact,
      activeId: id,
      modalVisible: !this.state.modalVisible,
    });
  };

  componentDidMount() {
    this.props.fetchContacts();
  }

  deleteContactsFunc = (event) => {
    event.preventDefault();
    this.props.deleteContact(this.state.activeId);
    this.setState({modalVisible : false})
  };

  render() {
    if (!this.props.contacts) {
      return (<div className="Loading">Loading...</div>)
    }

    return (
      <div className="Home">
        <div className="HomeItems">
          {Object.values(this.props.contacts).map((contact, index) => {
            const keys = Object.keys(this.props.contacts);

            return (
              <HomeItem
                image={contact.photo}
                name={contact.name}
                key={index}
                clicked={() => this.toggleModal(keys[index], contact)}
              />
            )
          })}
        </div>
        {this.state.modalVisible ? <Modal
          image={this.state.contact.photo}
          name={this.state.contact.name}
          phone={this.state.contact.phone}
          email={this.state.contact.email}
          editLink={"/" + this.state.activeId + '/edit'}
          deleteButton={event => this.deleteContactsFunc(event)}
          closeModal={() => this.setState({modalVisible : false})}
        /> : null}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  contacts: state.contacts,
});

const mapDispatchToProps = dispatch => ({
  fetchContacts: () => dispatch(fetchContacts()),
  deleteContact: (id) => dispatch(deleteContact(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
