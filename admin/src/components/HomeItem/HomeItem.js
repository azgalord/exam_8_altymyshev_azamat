import React from 'react';

import './HomeItem.css';

const HomeItem = props => (
  <div onClick={props.clicked} className="HomeItem">
    <img src={props.image} alt=""/>
    <p>{props.name}</p>
  </div>
);

export default HomeItem;
