import React from 'react';

import './Modal.css';
import {Link} from "react-router-dom";

const Modal = props => {
  return (
    <div className="ModalBg">
      <div className="Modal">
        <div className="ModalInfo">
          <img src={props.image} alt=""/>
          <div className="ModalText">
            <h1>{props.name}</h1>
            <span><b>Tel: </b> {props.phone}</span>
            <span><b>Email: </b> {props.email}</span>
          </div>
        </div>
        <div className="ModalButtons">
          <Link to={props.editLink}>Edit</Link>
          <button onClick={props.deleteButton}>Delete</button>
        </div>
        <button onClick={props.closeModal} className="ModalClose">x</button>
      </div>
    </div>
  );
};

export default Modal;
