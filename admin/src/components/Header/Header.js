import React from 'react';
import {Link} from 'react-router-dom';

import './Header.css';

const Header = () => {
  return (
    <header className="Header">
      <Link style={{background: 'transparent', color: '#000'}} to="/"><h2>Contacts</h2></Link>
      <Link to="/add">Add new contact</Link>
    </header>
  );
};

export default Header;
