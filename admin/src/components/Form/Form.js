import React from 'react';

import './Form.css';

const Form = props => {
  return (
    <div className="Form">
      <h3>{props.formTitle}t</h3>
      <form>
        <div className="FormItem">
          <label htmlFor="name">Name</label>
          <input
            value={props.inputValues.name}
            onChange={props.inputChange}
            type="text" id="name" name="name"/>
        </div>
        <div className="FormItem">
          <label htmlFor="phone">Phone</label>
          <input
            value={props.inputValues.phone}
            onChange={props.inputChange}
            type="text" id="phone" name="phone"/>
        </div>
        <div className="FormItem">
          <label htmlFor="email">Email</label>
          <input
            value={props.inputValues.email}
            onChange={props.inputChange}
            type="email" id="email" name="email"/>
        </div>
        <div className="FormItem">
          <label htmlFor="photo">Photo</label>
          <input
            value={props.inputValues.photo}
            onChange={props.inputChange}
            type="text" id="photo" name="photo"/>
        </div>
        <div className="FormItem">
          <label>Photo review</label>
          <img src={props.photoReview} alt=""/>
        </div>
        <div className="Buttons">
          <button onClick={props.saveButton}>Save</button>
          <button onClick={props.backButton}>Back to contacts</button>
        </div>
      </form>
    </div>
  );
};

export default Form;
