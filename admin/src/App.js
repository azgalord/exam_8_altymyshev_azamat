import React, { Component } from 'react';
import Header from "./components/Header/Header";
import {Route, Switch} from "react-router-dom";
import Home from "./containers/Home/Home";
import AddPage from "./containers/AddPage/AddPage";
import EditPage from "./containers/EditPage/EditPage";

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/add" exact component={AddPage}/>
          <Route path="/:id/edit" exact component={EditPage}/>
        </Switch>
      </div>
    );
  }
}

export default App;
