import axios from '../../axios-admin';
import {CONTACTS_SUCCESS, ONE_CONTACT_SUCCESS} from "./actionTypes";

export const contactsSuccess = contacts => ({type: CONTACTS_SUCCESS, contacts});
export const oneContactSuccess = contact => ({type: ONE_CONTACT_SUCCESS, contact});

export const saveContact = (contact) => {
  return dispatch => {
    axios.post('/allContacts.json', contact).then(() => {
        dispatch(fetchContacts())
    })
  }
};

export const sendChangedContact = (id, data) => {
  return dispatch => {
    axios.put('/allContacts/' + id + '.json', data).then(() => {
      dispatch(fetchContacts())
    })
  }
};

export const deleteContact = (id) => {
  console.log(id);
  return dispatch => {
    axios.delete('/allContacts/' + id + '.json').then(() => {
      dispatch(fetchContacts())
    })
  }
};

export const fetchContacts = () => {
  return dispatch => {
    axios.get('/allContacts.json').then(
      response => dispatch(contactsSuccess(response.data)),
    )
  }
};

export const fetchOneContact = id => {
  return dispatch => {
    axios.get('/allContacts/' + id + '.json').then(
      response => dispatch(oneContactSuccess(response.data))
    )
  }
};
