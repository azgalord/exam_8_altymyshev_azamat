import {CONTACTS_SUCCESS, ONE_CONTACT_SUCCESS} from "../actions/actionTypes";

const initialState = {
  contacts: null,
  oneContact: null,
};

const adminReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONTACTS_SUCCESS:
      return {
        ...state,
        contacts: action.contacts,
      };
    case ONE_CONTACT_SUCCESS:
      return {
        ...state,
        oneContact: action.contact,
      };
    default:
      return state;
  }
};

export default adminReducer;
