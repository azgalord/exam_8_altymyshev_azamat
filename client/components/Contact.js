import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';

const styles = StyleSheet.create({
  contact: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: 'lightgrey',
    borderWidth: 1,
    borderStyle: 'solid',
    paddingHorizontal: 5,
    paddingTop: 5,
    paddingBottom: 5,
    borderRadius: 15,
    marginBottom: 15,
    alignItems: 'center',
  },
});

const Contact = props => (
  <TouchableOpacity onPress={props.pressed}>
    <View style={styles.contact}>
      <Image source={{uri: props.image}} style={{width: 60, height: 60, borderRadius: 5}}/>
      <Text>{props.text}</Text>
    </View>
  </TouchableOpacity>
);

export default Contact;
