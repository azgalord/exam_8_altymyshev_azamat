import React from 'react';
import {StyleSheet, Modal, Image, View, Text, TouchableOpacity} from 'react-native';

const styles = StyleSheet.create({
  modal: {
    backgroundColor: '#ffffff',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    paddingHorizontal: 30,
    paddingTop: 70,
  },
  contacts: {
    textDecorationColor: 'blue',
    textDecorationStyle: 'solid',
    color: 'blue',
  }
});

const ModalComponent = props => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
    >
      <View style={styles.modal}>
        <Text style={{marginBottom: 40}}>Name: {props.name}</Text>
        <Image style={{width: 150, height: 150, marginBottom: 40}} source={{uri: props.image}}/>
        <Text style={styles.contacts}>Phone: {props.phone}</Text>
        <Text style={styles.contacts}>Email: {props.email}</Text>
        <TouchableOpacity style={{marginTop: 100}} onPress={props.back}>
          <View style={{backgroundColor: 'black', height: 50, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{color: 'white', textAlign: 'center'}}>Back to list</Text>
          </View>
        </TouchableOpacity>
      </View>
    </Modal>
  )
};

export default ModalComponent;
