import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';

import axios from './axios-client';
import Contact from "./components/Contact";
import ModalComponent from "./components/Modal";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingTop: 60,
    paddingHorizontal: 10,
  },
});

export default class App extends React.Component {
  state = {
    contacts: null,
    modal: false,
    thisContacts: {},
    thisContact: {},
  };

  componentDidMount() {
    axios.get('/allContacts.json').then(
      response => this.setState({contacts: response.data})
    )
  }

  getValues = (item) => {
    const keys = Object.keys(item);
    const values = Object.values(item);
    for (let key in keys) {
      values[key].id = keys[key];
    }

    return values;
  };

  modalToggle = () => {
    this.setState({modal: !this.state.modal});
  };

  openModalFunc = (event, contact) => {
    event.preventDefault();
    this.setState({thisContact : contact});
    this.modalToggle();
  };

  _keyExtractor = (item, index) => item.name;

  render() {
    if (!this.state.contacts) {
      return (<View style={styles.container}><Text style={{textAlign: 'center'}}>Loading...</Text></View>)
    }
    return (
      <View style={styles.container}>
        <FlatList
          data={this.getValues(this.state.contacts)}
          keyExtractor={this._keyExtractor}
          renderItem={({item}) => {
            // console.log(item);
            return (
              <Contact
                image={item.photo}
                text={item.name}
                pressed={event => this.openModalFunc(event, item)}
              />
            )
          }}
        />
        {this.state.modal ? <ModalComponent
          name={this.state.thisContact.name}
          image={this.state.thisContact.photo}
          phone={this.state.thisContact.phone}
          email={this.state.thisContact.email}
          back={this.modalToggle}
        /> : null}
      </View>
    );
  }
}
